
#include "MyGameEngine.h"
#include <iostream>
#include <time.h>
#include <ctime>
#include <stdio.h>
#include <ctime>
#include <ratio>
#include <chrono>

void MyGameEngine::idle() {
    if (user->modejeu == 1) {
        std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
        std::chrono::high_resolution_clock::time_point t2;
        acc++;
        srand(time(NULL));
        if (acc == vague->tick) {
            acc = 0;
            int r = (1 + (std::rand() % (cases->size() - 1)));
            float y = ((*cases)[r]->posY);
            int t = 1 + (rand() % (int) (3));
            while (t != 0) {
                if (vague->restant == 0) {
                    int numvague = user->numvague;
                    int t1 = vague->type1;
                    int t2 = vague->type2;
                    int t3 = vague->type3;
                    int t4 = vague->tick;
                    t1 = 10 * numvague;
                    t2 = 5 * numvague;
                    if (numvague > 4) {
                        t3 = numvague;
                    }
                    if (t4 >= 20) {
                        t4 = t4 - 5;
                    }
                    vague->setVague(t1, t2, t3, t4);
                    user->setVague();
                    t = 0;
                } else if (t == 1) {
                    if (vague->type1 == 0) {
                        t = 2;
                    } else {
                        vague->settype1();
                        vague->setrestant();
                        astds->push_back(new AsteroidPetit(0.5f - 0.03f, y + 0.09f));
                        t = 0;
                    }
                } else if (t == 2) {
                    if (vague->type2 == 0) {
                        t = 3;
                    } else {
                        vague->settype2();
                        vague->setrestant();
                        astds->push_back(new AsteroidMoyen(0.5f - 0.1f, y + 0.09f));
                        t = 0;
                    }
                } else if (t == 3) {
                    if (vague->type3 == 0) {
                        t = 1;
                    } else {
                        vague->settype3();
                        vague->setrestant();
                        astds->push_back(new AsteroidGrand(0.5f - 0.18f, y + 0.09f));
                        t = 0;
                    }
                }
            }
        }
        for (int i = 0; i < vaiss->size(); i++) {
            (*vaiss)[i]->tick();
            if ((*vaiss)[i]->vie <= 0) {
                int c = (*vaiss)[i]->cases;
                (*cases)[c]->libre = 1;
                vaiss->erase(vaiss->begin() + i);
            } else if (!(((*vaiss)[i]->click) % (4000 / ((*vaiss)[i]->rechargement)))) {
                misls->push_back(new Missile((*vaiss)[i]->posX, (*vaiss)[i]->posY - 0.005f, 0.03f, 0.01f, (*vaiss)[i]->vitesse, (*vaiss)[i]->red, (*vaiss)[i]->green, (*vaiss)[i]->blue, (*vaiss)[i]->force));
                (*vaiss)[i]->click = 0;
            }

        }

        for (int i = 0; i < misls->size(); i++) {
            (*misls)[i]->tick();
            if ((*misls)[i]->posX > (0.5f - 0.03f)) {
                misls->erase(misls->begin() + i);
            } else {
                for (int j = 0; j < astds->size(); j++) {
                    if ((((*misls)[i]->posX) >= (((*astds)[j]->posX)))&&
                            (((*misls)[i]->posX) <= (((*astds)[j]->posX) + 0.18f))&&
                            (((*misls)[i]->posY) >= (((*astds)[j]->posY) - 0.08f))&&
                            (((*misls)[i]->posY) <= (((*astds)[j]->posY) + 0.08f))
                            ) {
                        (*astds)[j]->vie -= ((*misls)[i]->force);
                        misls->erase(misls->begin() + i);
                        j = astds->size();
                    }
                }
            }
        }
        for (int i = 0; i < astds->size(); i++) {
            (*astds)[i]->tick();
            if ((*astds)[i]->vie <= 0) {
                user->gainScore((*astds)[i]->score);
                user->gainMoney((*astds)[i]->butin);
                astds->erase(astds->begin() + i);
            } else if ((*astds)[i]->posX<-1.0f) {
                astds->erase(astds->begin() + i);
                user->setMode(-1);
            } else {
                for (int j = 0; j < vaiss->size(); j++) {
                    if (
                            (((*astds)[i]->posX) >= (((*vaiss)[j]->posX) - 0.04f))
                            &&
                            (((*astds)[i]->posX) <= (((*vaiss)[j]->posX) + 0.04f))
                            &&
                            (((*astds)[i]->posY) >= (((*vaiss)[j]->posY) - 0.04f))
                            &&
                            (((*astds)[i]->posY) <= (((*vaiss)[j]->posY) + 0.04f))
                            ) {
                        (*vaiss)[j]->vie -= 50;
                        astds->erase(astds->begin() + i);
                        j = vaiss->size();
                    }
                }
            }
        }
        t2 = std::chrono::high_resolution_clock::now();
        while (std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() <= 40) {
            t2 = std::chrono::high_resolution_clock::now();
        }
    }
}
