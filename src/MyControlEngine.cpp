#include "MyControlEngine.h"

void MyControlEngine::MouseCallback(int button, int state, int x, int y) {
    int width=game->wWIDTH;
    int height=game->wHEIGHT;
    int sepx=width-(width/4);
    int sepy=height-(height/10);
    if (user->modejeu == 1) {
        if ((x > 0)&&(x < sepx)&&(y > 0)&&(y < sepy)) {
            int i = (x / (sepx/10))+((y / (sepy/10))*10);
            if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
                if ((((*cases)[i]->libre) == 1)&&(((i % 10) >= 0)&&((i % 10) <= 1))) {
                    float xv = (((*cases)[i]->posX)+(((*cases)[i]->width) / 2.0));
                    float yv = (((*cases)[i]->posY)+(((*cases)[i]->height) / 2.0));
                    if ((user->type == 2)&&((user->argent - 800) >= 0)) {
                        vaiss->push_back(new Destroyer(xv, yv, i));
                        user->gainMoney(-800);
                        (*cases)[i]->libre = 0;
                    } else if ((user->type == 3)&&((user->argent - 1500) >= 0)) {
                        vaiss->push_back(new DeathStar(xv, yv, i));
                        user->gainMoney(-1500);
                        (*cases)[i]->libre = 0;
                    } else if ((user->type == 1)&&((user->argent - 200) >= 0)) {
                        vaiss->push_back(new Chasseur(xv, yv, i));
                        user->gainMoney(-200);
                        (*cases)[i]->libre = 0;
                    }

                }
            } else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
                if ((((*cases)[i]->libre) == 0)
                        &&(
                        ((i % 10) >= 0)&&((i % 10) <= 1)
                        )) {
                    for (int j = 0; j < vaiss->size(); j++) {
                        int c = (*vaiss)[j]->cases;
                        if (c == i) {
                            (*cases)[c]->libre = 1;
                            int c = ((*vaiss)[j]->cout / 2);
                            user->gainMoney(c);
                            vaiss->erase(vaiss->begin() + j);
                        }

                    }
                }
            }
        } else if ((x > 0)&&(x < (width/4))&&(y > sepy)&&(y < height)) {
            user->setType(1);
        } else if ((x > (width/4))&&(x < 2*(width/4))&&(y > sepy)&&(y < height)) {
            user->setType(2);
        } else if ((x > 2*(width/4))&&(x < 3*(width/4))&&(y > sepy)&&(y < height)) {
            user->setType(3);
        } else if ((x > 3*(width/4))&&(x < width)&&(y > sepy)&&(y < height)) {
            while (vaiss->size() >= 1) {
                vaiss->erase(vaiss->begin());
            }
            while (astds->size() >= 1) {
                astds->erase(astds->begin());
            }
            while (misls->size() >= 1) {
                misls->erase(misls->begin());
            }
            exit(EXIT_SUCCESS);
        }
    }
}

void MyControlEngine::KeyboardCallback(unsigned char key, int x, int y) {
    if (user->modejeu == 0) {
        user->setMode(1);
    } else if (user->modejeu == -1) {
        while (vaiss->size() >= 1) {
            vaiss->erase(vaiss->begin());
        }
        while (astds->size() >= 1) {
            astds->erase(astds->begin());
        }
        while (misls->size() >= 1) {
            misls->erase(misls->begin());
        }
        for (int i = 0; i < cases->size(); i++) {
		(*cases)[i]->libre=1;
	}
        vague->reset();
        user->reset();
        user->setMode(0);
    }
}
