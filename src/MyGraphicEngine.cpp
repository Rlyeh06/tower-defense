
#include "MyGraphicEngine.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cstring>
using namespace std;
void MyGraphicEngine::reshape(int winWidth_, int winHeight_){
   game->wWIDTH=winWidth_;
   game->wHEIGHT=winHeight_;
}
void MyGraphicEngine::Draw(){
    if (user->modejeu==1){
	for (int i = 0; i < cases->size(); i++) {
		(*cases)[i]->draw();
	}
	float width=(*cases)[1]-> width;
        float height=(*cases)[1]->height;
	for (int i = 1; i <= 2; i++){
		GraphicPrimitives::drawLine2D(-1.0f+(i*width),-1.0f,-1.0f+(i*width),1.0f,0.0f,0.0f, 0.0f,0.0f);
	}
	for (int i = 1; i <= 9; i++){
		GraphicPrimitives::drawLine2D(-1.0f,1.0f-(i*height),1.0f,1.0f-(i*height),0.0f,0.0f, 0.0f,0.0f);
	}
	for (int i = 0; i < vaiss->size(); i++) {
		(*vaiss)[i]->draw();
	}
	for (int i = 0; i < astds->size(); i++) {
		(*astds)[i]->draw();
	}
	for (int i = 0; i < misls->size(); i++) {
		(*misls)[i]->draw();
	}
	user->draw();
    }else if (user->modejeu==-1){
        string str1="GAME OVER";
        string str2="Score:"+to_string(user->score);
        string str3="Appuyer sur n'importe quelle touche pour continuer";
        GraphicPrimitives::drawText2D(&str1[0],0.0f-0.15f,0.0f+0.2f,1.0f,0.0f,0.0f,1.0f);
        GraphicPrimitives::drawText2D(&str2[0],0.0f-0.1f,0.0f,1.0f,0.0f,0.0f,1.0f);
        GraphicPrimitives::drawText2D(&str3[0],0.0f-0.5f,0.0f-0.2f,1.0f,1.0f,1.0f,1.0f);
    }else if (user->modejeu==0){
        string str4="Jeu de defense";
        string str5="Vous voila Grand Amiral de la flotte de l'empire, vous avez poursuivi les rebelles jusqu'ici.";
        string str6="Il s'avere que c'etait un piege! Vous vous retrouvez dans un champ d'asteroides.";
        string str7="Appuyer sur n'importe quelle touche pour continuer.";
        string str8="Pour placer un de vos vaisseaux sur la carte, sélectionner le type voulu avec un clic gauche,";
        string str9="puis cliquer sur une case libre. Vous pouvez detruire l'un de vos vaisseaux deja en place.";
        string str10="Pour cela faites un clic droit dessus, la moitie de sa valeur vous sera recreditée.";
        string str11="Voici vos vaisseaux:";
        GraphicPrimitives::drawText2D(&str4[0],-0.2f,0.9f,1.0f,1.0f,1.0f,1.0f);
        GraphicPrimitives::drawText2D(&str5[0],-0.9f,0.8f,1.0f,1.0f,0.0f,1.0f);
        GraphicPrimitives::drawText2D(&str6[0],-0.9f,0.7f,1.0f,1.0f,0.0f,1.0f);
        GraphicPrimitives::drawText2D(&str8[0],-0.9f,0.6f,1.0f,1.0f,0.0f,1.0f);
        GraphicPrimitives::drawText2D(&str9[0],-0.9f,0.5f,1.0f,1.0f,0.0f,1.0f);
        GraphicPrimitives::drawText2D(&str10[0],-0.9f,0.4f,1.0f,1.0f,0.0f,1.0f);
        GraphicPrimitives::drawText2D(&str11[0],-0.9f,0.3f,1.0f,1.0f,0.0f,1.0f);
        float posiX=-0.85f;
        float posiY=0.2f;
        GraphicPrimitives::drawFillTriangle2D(posiX - 0.05f, posiY + 0.05f,posiX + 0.05f, posiY,posiX-0.05f,posiY-0.05f,0.0f,0.0f,1.0f);
        string str12="Chasseur: vie moyenne et une puissance de feu reduite, qu'il compense par sa vitesse";
        GraphicPrimitives::drawText2D(&str12[0],posiX+0.07f,posiY,1.0f,1.0f,1.0f,1.0f);
        posiY=posiY-0.15f;
        GraphicPrimitives::drawFillTriangle2D(posiX - 0.05f, posiY + 0.05f,posiX + 0.05f, posiY,posiX-0.05f,posiY-0.05f,0.0f,1.0f,0.0f);
        string str13="Destroyer: beaucoup de vie et puissance elevee, tout comme son cout";
        GraphicPrimitives::drawText2D(&str13[0],posiX+0.07f,posiY,1.0f,1.0f,1.0f,1.0f);
        posiY=posiY-0.15f;
        GraphicPrimitives::drawFillTriangle2D(posiX - 0.05f, posiY + 0.05f,posiX + 0.05f, posiY,posiX-0.05f,posiY-0.05f,1.0f,0.0f,0.0f);
        string str14="Death Star: puissance incarnee, demande des moyens colossaux, rien ne peut resister";
        string str15="a un de ses tirs, mais est lent et a une faible vie a cause d une faille dans le";
        string str16="systeme de refroidissement";
        GraphicPrimitives::drawText2D(&str14[0],posiX+0.07f,posiY,1.0f,1.0f,1.0f,1.0f);
        posiY=posiY-0.06f;
        GraphicPrimitives::drawText2D(&str15[0],posiX+0.07f,posiY,1.0f,1.0f,1.0f,1.0f);
        posiY=posiY-0.06f;
        GraphicPrimitives::drawText2D(&str16[0],posiX+0.07f,posiY,1.0f,1.0f,1.0f,1.0f);
        posiY=posiY-0.08f;
        string str17="Voici vos ennemis:";
        GraphicPrimitives::drawText2D(&str17[0],-0.9f,posiY,1.0f,1.0f,0.0f,1.0f);
        posiY=posiY-0.21f;
        GraphicPrimitives::drawFillRect2D(-0.9f,posiY,0.18f,0.18f,1.0f,1.0f,0.0f,1.0f);
        string str18="L'exterminateur, son nom n'appelle pas a une plus ample description";
        GraphicPrimitives::drawText2D(&str18[0],-0.9f+0.19f,posiY+0.09f,1.0f,1.0f,1.0f,1.0f);
        posiY=posiY-0.13f;
        GraphicPrimitives::drawFillRect2D(-0.9f,posiY,0.1f,0.1f,0.0f,1.0f,1.0f,1.0f);
        string str19="Asteroide moyen, n'est genant qu'en groupe";
        GraphicPrimitives::drawText2D(&str19[0],-0.9f+0.13f,posiY+0.03f,1.0f,1.0f,1.0f,1.0f);
        posiY=posiY-0.06f;
        GraphicPrimitives::drawFillRect2D(-0.9f,posiY,0.03f,0.03f,1.0f,0.0f,1.0f,1.0f);
        string str20="Poussiere d'etoile que vous pouvez farmer pour recolter un beau butin";
        GraphicPrimitives::drawText2D(&str20[0],-0.9f+0.04f,posiY-0.01f,1.0f,1.0f,1.0f,1.0f);
        GraphicPrimitives::drawText2D(&str7[0],-0.7f,-0.9f,1.0f,1.0f,1.0f,1.0f);
    }
}
