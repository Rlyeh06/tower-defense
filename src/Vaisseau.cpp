
#include "Vaisseau.h"

void Vaisseau::draw(){
	GraphicPrimitives::drawFillTriangle2D(posX - 0.05f, posY + 0.05f,posX + 0.05f, posY,posX-0.05f,posY-0.05f,red,green,blue);
	digits = 0;
	int number = vie;
	while(number!=0){
		number/=10;
		digits++;
	}
	sprintf (str, "%d", vie);
	GraphicPrimitives::drawText2D(str,posX,posY-0.07f,0.0f,1.0f,0.0f,0.0f);
}


void Vaisseau::tick(){
	click++;
}
