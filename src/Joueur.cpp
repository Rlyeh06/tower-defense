#include <iostream>
#include <cstring>
#include "Joueur.h"
using namespace std;
void Joueur::gainScore(int i){
	score+=i;
}
void Joueur::gainMoney(int i){
	argent+=i;
}
void Joueur::setType(int i){
	type=i;
}
void Joueur::setVague(){
	numvague++;
}
void Joueur::setMode(int i){
	modejeu=i;
}
void Joueur::reset(){
	score=0;
        numvague=1;
        argent=2000;
        type=1;
}
void Joueur::draw(){
	float posX,posY,r,g,b,a;
	posX=0.6f;
	posY=0.9f;
	r=1.0f;
	g=1.0f;
	b=1.0f;
	a=1.0f;
        string strArg="Argent:"+to_string(argent);
        string strScor="Score:"+to_string(score);
        string strVagu="Vague:"+to_string(numvague);
        string strRestan="Restant:"+to_string(vague->restant);
        GraphicPrimitives::drawText2D(&strArg[0],posX,posY,r,g,b,a);
        GraphicPrimitives::drawText2D(&strScor[0],posX,posY-0.1f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strVagu[0],posX,posY-0.2f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strRestan[0],posX,posY-0.3f,r,g,b,a);
        GraphicPrimitives::drawLine2D(-1.0f,-0.8f,1.0f,-0.8f,1.0f,1.0f, 1.0f,0.0f);
        GraphicPrimitives::drawLine2D(0.5f,1.0f,0.5f,-1.0f,1.0f,1.0f, 1.0f,0.0f);
        GraphicPrimitives::drawFillRect2D(0.5f, -0.8f,0.5f,-0.2f,0.5f,0.5f,0.5f);
        string quit="Quitter";
        GraphicPrimitives::drawText2D(&quit[0],0.7f,-0.92f,0.0f,0.0f,0.0f,a);
        GraphicPrimitives::drawLine2D(-0.5f,-0.8f,-0.5f,-1.0f,1.0f,1.0f, 1.0f,0.0f);
        GraphicPrimitives::drawLine2D(0.0f,-0.8f,0.0f,-1.0f,1.0f,1.0f, 1.0f,0.0f);
        GraphicPrimitives::drawLine2D(0.5f,-0.8f,0.5f,-1.0f,1.0f,1.0f, 1.0f,0.0f);
        float posiX=-0.94f;
        float posiY=-0.9f;
        string strDescri0="Chasseur";
        string strDescri1="Cout:200";
        string strDescri2="Vie:100";
        string strDescri3="Degat:50";
        string strDescri4="Vitesse:100";
        GraphicPrimitives::drawText2D(&strDescri0[0],posiX-0.06f,posiY+0.05f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strDescri1[0],posiX+0.2f,posiY+0.05f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strDescri2[0],posiX+0.05f,posiY-0.02f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strDescri3[0],posiX+0.23f,posiY-0.02f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strDescri4[0],posiX+0.1f,posiY-0.08f,r,g,b,a);
        GraphicPrimitives::drawFillTriangle2D(posiX - 0.05f, posiY + 0.05f,posiX + 0.05f, posiY,posiX-0.05f,posiY-0.05f,0.0f,0.0f,1.0f);
        posiX+=0.5f;
        strDescri0="Destroyer";
        strDescri1="Cout:800";
        strDescri2="Vie:200";
        strDescri3="Degat:200";
        strDescri4="Vitesse:80";
        GraphicPrimitives::drawText2D(&strDescri0[0],posiX-0.06f,posiY+0.05f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strDescri1[0],posiX+0.2f,posiY+0.05f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strDescri2[0],posiX+0.05f,posiY-0.02f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strDescri3[0],posiX+0.23f,posiY-0.02f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strDescri4[0],posiX+0.1f,posiY-0.08f,r,g,b,a);
        GraphicPrimitives::drawFillTriangle2D(posiX - 0.05f, posiY + 0.05f,posiX + 0.05f, posiY,posiX-0.05f,posiY-0.05f,0.0f,1.0f,0.0f);
        posiX+=0.5f;
        strDescri0="Death Star";
        strDescri1="Cout:1500";
        strDescri2="Vie:10";
        strDescri3="Degat:4999";
        strDescri4="Vitesse:1";
        GraphicPrimitives::drawText2D(&strDescri0[0],posiX-0.06f,posiY+0.05f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strDescri1[0],posiX+0.2f,posiY+0.05f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strDescri2[0],posiX+0.05f,posiY-0.02f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strDescri3[0],posiX+0.2f,posiY-0.02f,r,g,b,a);
        GraphicPrimitives::drawText2D(&strDescri4[0],posiX+0.1f,posiY-0.08f,r,g,b,a);
        GraphicPrimitives::drawFillTriangle2D(posiX - 0.05f, posiY + 0.05f,posiX + 0.05f, posiY,posiX-0.05f,posiY-0.05f,1.0f,0.0f,0.0f);
        if (type==1){
            GraphicPrimitives::drawFillRect2D(-0.55f, -0.95f,0.03f,-0.03f,1.0f,1.0f,0.0f);
        }else if (type==2){
            GraphicPrimitives::drawFillRect2D(-0.05f, -0.95f,0.03f,-0.03f,1.0f,1.0f,0.0f);
        }else if (type==3){
            GraphicPrimitives::drawFillRect2D(0.45f, -0.95f,0.03f,-0.03f,1.0f,1.0f,0.0f);
        }
}
