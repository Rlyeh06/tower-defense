#include <iostream>
#include "Engine.h"

#include "MyGraphicEngine.h"
#include "MyGameEngine.h"
#include "MyControlEngine.h"
#include "Joueur.h"
#include "Vague.h"
#include "Fenetre.h"

int main(int argc, char * argv[])
{
	
	Engine e(argc,argv);
	
	std::vector<Case *> cases;
	std::vector<Vaisseau *> vaiss;
	std::vector<Asteroid *> astds;
	std::vector<Missile *> misls;
	Joueur user;
        Vague vague;
        Fenetre game;
        game= Fenetre(800,600);
        vague= Vague(15,0,0,15,200);
	user= Joueur(2000,1,0,&vague,0);
	float y=1.0f;
	float x=-1.0f;
        float widthx = 1.5f/10.f;
        float heighty = 1.8f/10.f;
	for (int i = 0; i < 10; i++){
		x=-1.0f;
                y-=heighty;
		for (int j = 0; j < 10; j++){
			cases.push_back(new Case(x, y,widthx,heighty, 1.0f, 1.0f, 1.0f,1));
	  		x+=widthx;
		}
		
         	}
        
	GameEngine * gme = new MyGameEngine(&cases,&vaiss,&astds,&misls,&user,&vague);
	ControlEngine * ce = new MyControlEngine(&cases,&vaiss,&astds,&misls,&user,&vague,&game);
        GraphicEngine * ge = new MyGraphicEngine(&cases,&vaiss,&astds,&misls,&user,&vague,&game);
        e.setGraphicEngine(ge);
	e.setGameEngine(gme);
	e.setControlEngine(ce);
	e.start();
	return 0;
}
