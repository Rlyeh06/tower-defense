#pragma once
#include "GraphicPrimitives.h"
#include "Vague.h"

class Joueur {
	Vague *vague;
public:
	Joueur(int argent_ = 1000,
	int numvague_ = 1,
	int score_ = 0,
        Vague *vague_=0,
        int modejeu_ =0,
        int restant_ =35,
        int type_=1):
	argent(argent_),
	numvague(numvague_),
	score(score_),
        vague(vague_),
        modejeu(modejeu_),
        restant(restant_),
        type(type_)
	{}
	int argent;
	int numvague;
	int score;
        int type;
        int restant;
        int modejeu;
	void gainScore(int i);
	void gainMoney(int i);
        void setType(int i);
	void draw();
        void setVague();
        void setMode(int i);
        void reset();
};
