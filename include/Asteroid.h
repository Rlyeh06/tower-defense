#pragma once
#include "GraphicPrimitives.h"

class Asteroid {
	
public:
	Asteroid(float posX_ = 1.0f,float posY_ = 1.0f, float width_ = 0.18f,float height_ = 0.18f,float VposX_=0.001f, float red_ =1.0f, float green_ =0.0f, float blue_ =0.0f, int vie_=15000, int viemax_=15000, int score_=100, int butin_=100):posX(posX_),posY(posY_),width(width_),height(height_),VposX(VposX_),red(red_),green(green_),blue(blue_),vie(vie_),viemax(viemax_),score(score_),butin(butin_){}
	float posX,posY,width,height;
	float VposX;
	float red;
	float blue;
	float green;
	int vie;
        int viemax;
        int score;
        int butin;
	void draw();
	void tick();
};
