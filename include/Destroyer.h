#pragma once
#include "GraphicPrimitives.h"
#include "Vaisseau.h"

class Destroyer : public Vaisseau {
	
public:
	Destroyer(float posX,float posY, int cases): Vaisseau(posX,posY,0.0f,1.0f,0.0f,cases,200,0,0.008f,80,200,800){};
};
