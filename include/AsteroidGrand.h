#pragma once
#include "GraphicPrimitives.h"
#include "Asteroid.h"

class AsteroidGrand : public Asteroid {
	
public:
    AsteroidGrand(float posX,float posY): Asteroid(posX,posY,0.18f,0.18f,0.0001f,1.0f,1.0f,0.0f,15000,15000,10000,1000){};

};