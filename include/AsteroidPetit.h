#pragma once
#include "GraphicPrimitives.h"
#include "Asteroid.h"

class AsteroidPetit : public Asteroid {
	
public:
    AsteroidPetit(float posX,float posY): Asteroid(posX,posY,0.03f,0.03f,0.01f,1.0f,0.0f,1.0f,100,100,100,50){};
};

