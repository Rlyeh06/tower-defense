#pragma once
class Vague {
public:
    Vague(int type1_=30, int type2_=5, int type3_=0, int restant_=35, int tick_=90):
    type1(type1_),type2(type2_),type3(type3_),restant(restant_),tick(tick_){}
    int type1;
    int type2;
    int type3;
    int restant;
    int tick;
    void setrestant();
    void settype1();
    void settype2();
    void settype3();
    void reset();
    void setVague(int t1, int t2, int t3, int ticks);
};

