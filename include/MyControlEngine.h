#pragma once
#include "Engine.h"
#include "Case.h"
#include "Asteroid.h"
#include "Missile.h"
#include "Vaisseau.h"
#include "Destroyer.h"
#include "DeathStar.h"
#include "Chasseur.h"
#include "Joueur.h"
#include "Vague.h"
#include "Fenetre.h"

class MyControlEngine:public ControlEngine {
	std::vector<Case * > *cases;
	std::vector<Vaisseau *> *vaiss;
	std::vector<Asteroid *> *astds;
	std::vector<Missile *> *misls;
	Joueur *user;
        Vague *vague;
        Fenetre *game;
public:
	MyControlEngine(std::vector<Case * > * cases_,
	std::vector<Vaisseau *> * vaiss_,
	std::vector<Asteroid *> * astds_,
	std::vector<Missile *> * misls_,
	Joueur *joueur_,
        Vague *vague_,
        Fenetre *game_):
	cases(cases_),
	vaiss(vaiss_),
	astds(astds_),
	misls(misls_),
	user(joueur_),
        vague(vague_),
        game(game_){}
	virtual void MouseCallback(int button, int state, int x, int y) ;
        virtual void KeyboardCallback(unsigned char key,int x, int y) ;
};
