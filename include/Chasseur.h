#pragma once
#include "GraphicPrimitives.h"
#include "Vaisseau.h"

class Chasseur : public Vaisseau {
	
public:
	Chasseur(float posX,float posY, int cases): Vaisseau(posX,posY,0.0f,0.0f,1.0f,cases,100,0,0.1f,100,50,200){};
};
