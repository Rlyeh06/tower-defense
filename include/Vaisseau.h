#pragma once
#include "GraphicPrimitives.h"
#include "Missile.h"

class Vaisseau {
public:
	Vaisseau(float posX_ = 0.0f,
	float posY_ = 0.0f,
	float red_ =0.0f,
	float green_ =0.0f,
	float blue_ =0.0f,
	int cases_ =-1,
	int vie_ =100,
	int click_=0,
	float vitesse_=0.01f,
	int rechargement_=80,
	int force_=25,
	int cout_=100):
	posX(posX_),
	posY(posY_),
	red(red_),
	green(green_),
	blue(blue_),
	cases(cases_),
	vie(vie_),
	str(new char[digits]),
	click(click_),
	vitesse(vitesse_),
	rechargement(rechargement_),
	force(force_),
	cout(cout_){}
	float posX,posY;
	float red;
	float blue;
	float green;
	void draw();
	void tick();
	int cases;
	int vie;
	char * str;
	int digits;
	int click;
	float vitesse;
	int rechargement;
	int force;
	int cout;
};
