#pragma once
#include "GraphicPrimitives.h"
#include "Asteroid.h"

class AsteroidMoyen : public Asteroid {
	
public:
    AsteroidMoyen(float posX,float posY): Asteroid(posX,posY,0.1f,0.1f,0.005f,0.0f,1.0f,1.0f,1000,1000,1000,300){};

};