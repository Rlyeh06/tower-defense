#pragma once
#include "GraphicPrimitives.h"

class Case {
public:
	Case(float posX_ = 0.0f,
	float posY_ = 0.0f,
	float width_=0.1f,
	float height_=0.1f,
	float red_ =1.0f,
	float green_ =1.0f,
	float blue_ =1.0f,
	int libre_=1):
	posX(posX_),
	posY(posY_),
	width(width_),
	height(height_),
	red(red_),
	green(green_),
	blue(blue_),
	libre(libre_){}
	float posX,posY;
	float width;
	float height;
	float red;
	float blue;
	float green;
	void draw();
	int libre;
};
