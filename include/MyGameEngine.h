#pragma once

#include "Engine.h"
#include "Case.h"
#include "Asteroid.h"
#include "AsteroidGrand.h"
#include "AsteroidMoyen.h"
#include "AsteroidPetit.h"
#include "Missile.h"
#include "Vaisseau.h"
#include "Joueur.h"
#include "Vague.h"

class MyGameEngine:public GameEngine {
	std::vector<Case * > *cases;
	std::vector<Vaisseau *> *vaiss;
	std::vector<Asteroid *> *astds;
	std::vector<Missile *> *misls;
	Joueur *user;
        Vague *vague;
public:
	int acc;
	MyGameEngine(std::vector<Case * > * cases_,
	std::vector<Vaisseau *> * vaiss_,
	std::vector<Asteroid *> * astds_,
	std::vector<Missile *> * misls_,
	Joueur *joueur_,
        Vague *vague_,
	int acc_=0):
	cases(cases_),
	vaiss(vaiss_),
	astds(astds_),
	misls(misls_),
	user(joueur_),
        vague(vague_),
	acc(acc_){}
	virtual void idle();
};
