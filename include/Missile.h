#pragma once
#include "GraphicPrimitives.h"

class Missile {
	
public:
	Missile(float posX_ = -1.0f,float posY_ = -1.0f,float width_ = 0.03f, float height_ = 0.01f,float VposX_ = 0.01f, float red_ =0.0f, float green_ =0.0f, float blue_ =0.0f,int force_=25):
	posX(posX_),posY(posY_),width(width_),height(height_),VposX(VposX_),red(red_),green(green_),blue(blue_),force(force_){}
	
	float posX,posY,width,height;
	float VposX;
	int force;
	float red;
	float blue;
	float green;
	void draw();
	void tick();
};
