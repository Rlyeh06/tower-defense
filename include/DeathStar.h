#pragma once
#include "GraphicPrimitives.h"
#include "Vaisseau.h"

class DeathStar : public Vaisseau {
	
public:
	DeathStar(float posX,float posY, int cases): Vaisseau(posX,posY,1.0f,0.0f,0.0f,cases,10,0,0.001f,7,4999,1500){};
};